package com.gitlab.quoopie.Actions;

import com.gitlab.quoopie.Discordbot.Listeners.ChannelListener;
import com.gitlab.quoopie.Discordbot.DiscordBot;
import org.javacord.api.DiscordApi;
import org.javacord.api.entity.activity.ActivityType;
import org.javacord.api.entity.channel.ServerVoiceChannel;
import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;

public class EnableAction {

    public static void enable() {
        DiscordApi discordApi = DiscordBot.getInstance().getDiscordApi();
        discordApi.updateActivity(ActivityType.PLAYING, "Ish");
        ChannelListener channelListener = DiscordBot.getInstance().getChannelListener();

        if (!discordApi.getServerVoiceChannelMemberJoinListeners().contains(channelListener)) {
            discordApi.addServerVoiceChannelMemberJoinListener(channelListener);
        }

        for (Server server : discordApi.getServers()) {
            for (ServerVoiceChannel channel : server.getVoiceChannels()) {
                for (User user : channel.getConnectedUsers()) {
                    if (user.getId() == DiscordBot.getInstance().getTARGET() && server.getVoiceChannelById(DiscordBot.getInstance().getConfig().getTARGET_CHANNEL_ID()).isPresent()) {
                        user.move(server.getVoiceChannelById(DiscordBot.getInstance().getConfig().getTARGET_CHANNEL_ID()).get());
                    }
                }
            }
        }
    }

}
