package com.gitlab.quoopie.Actions;

import com.gitlab.quoopie.Discordbot.DiscordBot;
import org.javacord.api.DiscordApi;
import org.javacord.api.entity.activity.ActivityType;

public class DisableAction {
    public static void disable() {
        DiscordApi discordApi = DiscordBot.getInstance().getDiscordApi();
        discordApi.updateActivity(ActivityType.WATCHING, "Ish");
        if (discordApi.getServerVoiceChannelMemberJoinListeners().contains(DiscordBot.getInstance().getChannelListener())) {
            discordApi.removeListener(DiscordBot.getInstance().getChannelListener());
        }
    }
}
