package com.gitlab.quoopie.Discordbot.Listeners;

import com.gitlab.quoopie.Discordbot.DiscordBot;
import org.javacord.api.entity.channel.ServerVoiceChannel;
import org.javacord.api.entity.channel.ServerVoiceChannelBuilder;
import org.javacord.api.entity.server.Server;
import org.javacord.api.event.channel.server.ServerChannelDeleteEvent;
import org.javacord.api.listener.channel.server.ServerChannelDeleteListener;

public class ChannelDeleteListener implements ServerChannelDeleteListener {
    @Override
    public void onServerChannelDelete(ServerChannelDeleteEvent event) {
        if (event.getServer().getId() != DiscordBot.getInstance().getTARGET_SERVER_ID()) return;
        if (event.getChannel().getId() != DiscordBot.getInstance().getConfig().getTARGET_CHANNEL_ID()) return;

        Server server = event.getServer();
        ServerVoiceChannel channel = new ServerVoiceChannelBuilder(server).setName("Ish Land").setUserlimit(1).create().join();
        DiscordBot.getInstance().setTARGET_SERVER_ID(channel.getId());
    }
}
