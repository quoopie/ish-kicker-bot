package com.gitlab.quoopie.Discordbot.Listeners;

import com.gitlab.quoopie.Actions.EnableAction;
import com.gitlab.quoopie.Discordbot.DiscordBot;
import org.javacord.api.event.interaction.InteractionCreateEvent;
import org.javacord.api.interaction.SlashCommandInteraction;
import org.javacord.api.listener.interaction.InteractionCreateListener;

import java.util.Optional;

public class BanishListener implements InteractionCreateListener {
    @Override
    public void onInteractionCreate(InteractionCreateEvent event) {
        Optional<SlashCommandInteraction> interactionOptional = event.getSlashCommandInteraction();
        if (interactionOptional.isEmpty()) return;
        SlashCommandInteraction interaction = interactionOptional.get();
        if (!interaction.getFullCommandName().equals("banish")) return;
        if (interaction.getUser().getId() == DiscordBot.getInstance().getTARGET()) return;

        EnableAction.enable();
    }
}
