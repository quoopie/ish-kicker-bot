package com.gitlab.quoopie.Discordbot.Listeners;

import com.gitlab.quoopie.Discordbot.DiscordBot;
import org.javacord.api.event.channel.server.voice.ServerVoiceChannelMemberJoinEvent;
import org.javacord.api.listener.channel.server.voice.ServerVoiceChannelMemberJoinListener;

public class ChannelListener implements ServerVoiceChannelMemberJoinListener {
    @Override
    public void onServerVoiceChannelMemberJoin(ServerVoiceChannelMemberJoinEvent event) {
        final long ID = event.getUser().getId();
        final long TARGET = DiscordBot.getInstance().getTARGET();
        final long TARGET_CHANNEL_ID = DiscordBot.getInstance().getConfig().getTARGET_CHANNEL_ID();

        if (ID == TARGET && event.getServer().getVoiceChannelById(TARGET_CHANNEL_ID).isPresent()) {
            event.getUser().move(event.getServer().getVoiceChannelById(TARGET_CHANNEL_ID).get());
        }
    }
}
