package com.gitlab.quoopie.Discordbot;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.quoopie.Discordbot.Listeners.BanishListener;
import com.gitlab.quoopie.Discordbot.Listeners.ChannelDeleteListener;
import com.gitlab.quoopie.Discordbot.Listeners.ChannelListener;
import com.gitlab.quoopie.Discordbot.Listeners.PardonListener;
import lombok.Getter;
import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;
import org.javacord.api.entity.channel.ServerVoiceChannel;
import org.javacord.api.entity.channel.ServerVoiceChannelBuilder;
import org.javacord.api.entity.intent.Intent;
import org.javacord.api.entity.server.Server;
import org.javacord.api.interaction.SlashCommand;

import java.io.File;
import java.util.Optional;

public class DiscordBot {

    private static DiscordBot instance;

    @Getter
    private final long TARGET = 728601344838991872L;
    // ME private final long TARGET = 214375439911616512L;
    @Getter
    private Config config = new Config();

    @Getter
    private final long TARGET_SERVER_ID = 745679292431597658L;
    @Getter
    private final DiscordApi discordApi;
    @Getter
    private final ChannelListener channelListener = new ChannelListener();

    private final ObjectMapper om = new ObjectMapper();

    private DiscordBot() {
        final String DISCORD_TOKEN = "OTY3NDI2ODcyMzczODA5MjIz.YmQIfA.EDr_LOSvJghajZJdwIzkBSsCEh8";
        discordApi = new DiscordApiBuilder().setToken(DISCORD_TOKEN).setIntents(Intent.MESSAGE_CONTENT).login().join();
        Optional<Server> serverOptional = discordApi.getServerById(TARGET_SERVER_ID);
        if (serverOptional.isEmpty()) {
            System.out.println("Fatal Error: Could not find server!");
            System.exit(1);
        }
        Server server = serverOptional.get();

        SlashCommand.with("banish", "Banished Ish to Ish Land").createForServer(server).join();
        SlashCommand.with("pardon", "Pardons Ish from banishment").createForServer(server).join();

        discordApi.addServerVoiceChannelMemberJoinListener(channelListener);
        discordApi.addInteractionCreateListener(new BanishListener());
        discordApi.addInteractionCreateListener(new PardonListener());
        discordApi.addServerChannelDeleteListener(new ChannelDeleteListener());
        try {
            if (!new File("./config.json").exists()) {


                ServerVoiceChannel channel = new ServerVoiceChannelBuilder(server).setName("Ish Land").setUserlimit(1).create().join();
                setTARGET_SERVER_ID(channel.getId());
            }
            config = om.readValue(new File("./config.json"), Config.class);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static DiscordBot getInstance() {
        if (instance == null) instance = new DiscordBot();
        return instance;
    }

    public void setTARGET_SERVER_ID(long id) {
        config.setTARGET_CHANNEL_ID(id);
        try {
            om.writeValue(new File("./config.json"), config);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
