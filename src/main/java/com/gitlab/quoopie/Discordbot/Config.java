package com.gitlab.quoopie.Discordbot;

import lombok.Getter;
import lombok.Setter;

public class Config {
    public Config() {}

    public Config(long TARGET_CHANNEL_ID) {
        this.TARGET_CHANNEL_ID = TARGET_CHANNEL_ID;
    }
    @Getter @Setter
    public long TARGET_CHANNEL_ID;

}
